FROM nvidia/cuda:10.1-runtime

MAINTAINER Mohammed E. Amer <mohammed.e.amer@gmail.com>

RUN apt update && apt install -y python3-pip

RUN pip3 install --upgrade pip

RUN pip3 install torch torchvision torchaudio

RUN pip3 install numpy
RUN pip3 install scikit-learn
RUN pip3 install scipy
RUN pip3 install pandas
RUN pip3 install tables
RUN pip3 install pyyaml

RUN pip3 install pytest

WORKDIR /usr/src/

WORKDIR ./results/

ENTRYPOINT ["./run.sh"]
