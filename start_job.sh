#!/bin/bash

source ~/anaconda3/etc/profile.d/conda.sh
conda activate dl

python results/$EXP/run.py $SUBEXP $TAG
