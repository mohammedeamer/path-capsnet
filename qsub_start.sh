#!/bin/bash
#$ -V
#$ -N conv-caps
#$ -cwd
#$ -M hcxma1@nottingham.edu.my
#$ -m aes

./start_job.sh

# qsub example: EXP=<exp-name> SUBEXP=train TAG=<exp-tag> qsub qsub_start.sh
