#!/bin/bash

set -e

python3 $1/run.py "${@:2}"
