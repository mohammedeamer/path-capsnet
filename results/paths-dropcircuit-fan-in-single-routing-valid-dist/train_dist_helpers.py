import os
import logging as log

from logger import MainLogger as disklog

import torch as tch
import torch.distributed as dist

import utils
from optim_dist import DistOptim

def save_chkpnt(e, model, optim, path):
	tch.save({'e': e, 'model': model.state_dict(), 'optim': optim.state_dict()}, os.path.join(path,"chkpnt.pt"))

def load_chkpnt(path):

	path = os.path.join(path, 'chkpnt.pt')

	if os.path.exists(path):
		return tch.load(path)

	return None

def train_epoch(model, dist_optim, train_ds):

	rank = dist.get_rank()

	loss_avg = utils.AverageMeter()
	perform_avg = utils.AverageMeter()

	batches_count = len(train_ds)

	for b, (x, y) in enumerate(train_ds):

		model.train()

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		loss, perform = dist_optim.step(x, y)

		if rank == 0:

			loss_avg.update(loss, x.size(0))
			perform_avg.update(perform, x.size(0))

			log.info("batch: {}/{} -- train_loss: {}".format(b+1, batches_count, loss_avg.avg))

	if rank == 0:
		return loss_avg.avg, perform_avg.avg

	return None, None

def infer(model, perform_fn, valid_ds):

	rank = dist.get_rank()

	loss_avg = utils.AverageMeter()

	model.eval()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o = model(x)

		if rank == 0:

			loss = perform_fn(o[0], y)

			loss_avg.update(loss, x.size(0))

			log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	if rank == 0:
			return loss_avg.avg

	return None

def train(model, loss_fn, perform_fn, optim_fn, train_ds, valid_ds, epochs, results_path):

	rank = dist.get_rank()

	optim = optim_fn(model.parameters())

	dist_optim = DistOptim(model, optim, loss_fn, perform_fn)

	start_e = 0

	best_valid_err = None

	for e in range(start_e, epochs):

		log.info("epoch: {}/{}".format(e+1, epochs))

		train_err, train_perform = train_epoch(model, dist_optim, train_ds)
		valid_err = infer(model, perform_fn, valid_ds)

		save_flag = tch.zeros(1)

		if rank == 0:

			if best_valid_err is None or valid_err < best_valid_err:

				best_valid_err = valid_err

				save_flag[0] = 1

			disklog.log_train_loss(e, train_err)
			disklog.log_train_perform(e, train_perform, valid_err)

		dist.broadcast(tensor=save_flag, src=0)

		if save_flag == 1: disklog.save_model(model, rank)
