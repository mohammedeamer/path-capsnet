import torch as tch

from model import FCCapsLayer, squash

class CNN(tch.nn.Module):

	'''
		@kerns_chs:
			List of tuples. Each tuple is (kernel_size, out_channels, max_pool_size)

	'''
	def __init__(self, in_chs, kerns_chs):

		super(CNN, self).__init__()

		self.layers_modules = list()

		prev_chs = in_chs

		for k, chs, max_pool in kerns_chs:

			mods = [tch.nn.Conv2d(prev_chs, chs, k, padding=4), tch.nn.ReLU()]

			if max_pool is not None:
				mods.append(tch.nn.MaxPool2d(max_pool))

			self.layers_modules.append(tch.nn.Sequential(*mods))

			prev_chs = chs

		self.layers_modules = tch.nn.ModuleList(self.layers_modules)

	def forward(self, x):

		o = x

		for m in self.layers_modules:
			o = m(o)

		return o

class PathCapsNet(tch.nn.Module):
	'''
		@paths_dims:
			List of tuples. Each tuple is (kernel_size, path_width, path_depth, max_pool_size)

	'''
	def __init__(self, in_chs, paths_dims, pathcaps_dim, digitcaps_dim):

		super(PathCapsNet, self).__init__()

		self.in_chs = in_chs
		self.digitcaps_dim = digitcaps_dim
		self.pathcaps_dim = pathcaps_dim

		self.paths = list()

		for p_dims in paths_dims:

			depth = p_dims[2]

			assert depth > 1

			mp_idx = int(depth/2)
			kerns_chs = [[p_dims[0], p_dims[1], None] for _ in range(depth-1)]
			kerns_chs.append([p_dims[0], pathcaps_dim, None])
			kerns_chs[mp_idx][2] = p_dims[3]
			kerns_chs[-1][2] = p_dims[3]

			kerns_chs = map(tuple, kerns_chs)

			self.paths.append(CNN(in_chs=in_chs, kerns_chs=kerns_chs))

		self.paths = tch.nn.ModuleList(self.paths)
		self.digitcaps = FCCapsLayer(incaps_num=7*7*len(self.paths), incaps_dim=pathcaps_dim, outcaps_num=10, outcaps_dim=digitcaps_dim)

	def __calc_paths_output(self, x):

		paths_outputs = list()

		for path in self.paths:
			paths_outputs.append(path(x).view(x.size(0), -1, self.pathcaps_dim))

		o = squash(tch.cat(paths_outputs, 1), -1)
		return self.digitcaps(o)

	def forward(self, x):
		return self.__calc_paths_output(x)

