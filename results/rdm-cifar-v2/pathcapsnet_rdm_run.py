import os, sys, argparse, json, functools, logging

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

from rdm_utils import calc_rdm_corr, log_corr
from comb_dataset import CombDataset

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

NUM_WORKERS = 2

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/cifar-10/exp-data")

def run(exp_root, config_path, model_path, result_path, is_drop, subset_size):

	sys.path.insert(0, exp_root)

	from model import PathCapsNet

	with open(config_path, 'r') as f:
		config = json.load(f)

	model = functools.partial(PathCapsNet, in_channels=config['exp_params']['in_channels'], paths_num=config['exp_params']['paths_num'], pathcaps_dim=config['exp_params']['pathcaps_dim'], digitcaps_dim=config['exp_params']['digitcaps_dim'])

	if is_drop == 'True':
		model = model(drop_prob=config['exp_params']['drop_prob'])
	else:
		model = model()

	model.to(device)
	model.load_state_dict(tch.load(model_path, map_location=device))

	trans = transforms.Compose([transforms.ToTensor()])

	test_ds = dutils.Subset(dset.CIFAR10(root=ds_path, train=False, transform=trans, download=True), range(subset_size))
	test_ds = dutils.DataLoader(CombDataset(test_ds), shuffle=True, batch_size=256, num_workers=NUM_WORKERS)

	corr = calc_rdm_corr(model, config['exp_params']['paths_num'], test_ds)

	log_corr(result_path, corr)

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument('exp_root', action='store')
	parser.add_argument('config_path', action='store')
	parser.add_argument('model_path', action='store')
	parser.add_argument('result_path', action='store')
	parser.add_argument('is_drop', action='store')
	parser.add_argument('subset_size', action='store')

	args = parser.parse_args()

	run(args.exp_root, args.config_path, args.model_path, args.result_path, args.is_drop, int(args.subset_size))

