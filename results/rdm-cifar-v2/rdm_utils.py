import os, itertools, csv, logging

import torch as tch

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

def calc_rdm_corr(model, primcaps_num, ds_loader):

	with tch.no_grad():

		model.eval()

		batches_count = len(ds_loader)

		# calc each primary capsule RDMs

		rdms = []

		for batch_idx, (x_1, x_2) in enumerate(ds_loader):

			logging.info('RDMs - batch: {}/{}'.format(batch_idx+1, batches_count))

			x_1 = x_1.to(device)
			x_2 = x_2.to(device)

			primcaps_1 = model(x_1)[1]
			primcaps_1 = primcaps_1.reshape(primcaps_1.size(0), primcaps_num, -1)

			primcaps_2 = model(x_2)[1].reshape_as(primcaps_1)

			# (samples, primcaps_num)
			_rdms = (primcaps_1 - primcaps_2).pow(2.0).sum(dim=-1).sqrt()

			rdms.append(_rdms)

		# (primcaps_num, samples)
		rdms = tch.cat(rdms, dim=0).transpose(0,1)

		# calc primcaps corr

		primcaps_num = rdms.size(0)

		primcaps_indices = range(primcaps_num)
		primcaps_indices = list(itertools.combinations(primcaps_indices, 2))

		caps_pairs_count = len(primcaps_indices)

		corrs = []

		for caps_pair_idx, (idx_1, idx_2) in enumerate(primcaps_indices):

			logging.info('Corr - pair: {}/{}'.format(caps_pair_idx+1, caps_pairs_count))

			primcaps_1 = rdms[idx_1]
			primcaps_2 = rdms[idx_2]

			mu_1 = primcaps_1.mean()
			mu_2 = primcaps_2.mean()

			cov = (primcaps_1 - mu_1).mul(primcaps_2 - mu_2).mean()

			sig_1 = primcaps_1.std()
			sig_2 = primcaps_2.std()

			corr = cov/(sig_1*sig_2)
			corrs.append([caps_pair_idx, corr.item()])

		return corrs

def log_corr(path, rows):

	with open(os.path.join(path), 'w') as f:
		csv_writer = csv.writer(f)
		csv_writer.writerows(rows)

