import itertools

import torch.utils.data as dutils

class CombDataset(dutils.Dataset):

	def __init__(self, ds):

		self.x = [x for x, _ in ds]
		self.x = list(itertools.combinations(self.x, 2))

	def __len__(self):
		return len(self.x)

	def __getitem__(self, idx):
		return self.x[idx]

