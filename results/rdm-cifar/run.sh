#!/bin/bash

set -e

CAPSNET_EXEC=$1
PATHCAPSNET_EXEC=$2
EXPS_PATH=$3
RESULTS_PATH=$4/subset_size_$5
SUBSET_SIZE=$5

capsnet(){

				EXP_PATH=$1
				EXP_NAME=$(basename $EXP_PATH)

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $CAPSNET_EXEC $EXP_PATH $EXP_PATH/exp-results/$EXP_NAME/$t/model.pt $_RESULTS_PATH/corr.csv $SUBSET_SIZE
								fi

				done
}

pathcapsnet(){

				EXP_PATH=$1
				IS_DROP=$2
				EXP_NAME=$(basename $EXP_PATH)

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $PATHCAPSNET_EXEC $EXP_PATH $EXP_PATH/exp-results/$EXP_NAME/config.json  $EXP_PATH/exp-results/$EXP_NAME/$t/model.pt $_RESULTS_PATH/corr.csv $IS_DROP $SUBSET_SIZE

								fi
				done
}

for f in $EXPS_PATH/*;
do
				if [[ -d $f ]] && [[ $f == *cifar ]]
				then
								if [[ $f == *orig* ]];
								then

												capsnet $f

								elif [[ $f == *paths* ]]
								then
												IS_DROP='False'

												if [[ $f == *dropcircuit* ]]
												then
																IS_DROP='True'
												fi

												pathcapsnet $f $IS_DROP
								fi
				fi
done
