import math

import torch as tch
from torch.autograd import Variable
import torch.nn.functional as F

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

routing_iters = 3

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

class FCCapsLayer(tch.nn.Module):

	def __init__(self, incaps_num=None, incaps_dim=None, outcaps_num=None, outcaps_dim=None):

		super(FCCapsLayer, self).__init__()

		self.incaps_num = incaps_num
		self.incaps_dim = incaps_dim
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		# transformation matrices (incaps_num, outcaps_num, incaps_dim, outcaps_dim)

		stdv = 1. / math.sqrt(incaps_num)

		self.trans_matrices = tch.nn.Parameter(tch.Tensor(incaps_num, outcaps_num, incaps_dim, outcaps_dim).uniform_(-stdv, stdv), requires_grad=True)

	def __routing(self, pred_vectors, coupling_coeffs):

		norm_coupling_coeffs = F.softmax(coupling_coeffs, dim=2)

		norm_coupling_coeffs = norm_coupling_coeffs.unsqueeze(-1).expand(*pred_vectors.size())

		output = (norm_coupling_coeffs * pred_vectors).sum(dim=1)

		return squash(output, dim=-1)

	def forward(self, x):

		# x is (batch, incaps_num, incaps_dim)

		# x is (batch, incaps_num, outcaps_num, incaps_dim, 1)
		x = x.unsqueeze(2).unsqueeze(-1).expand(-1, -1, self.outcaps_num, -1, -1)

		# W is (batch, incaps_num, outcaps_num, outcaps_dim, incaps_dim)
		W = self.trans_matrices.expand(x.size()[0], *self.trans_matrices.size()).transpose(-1,-2)

		# calculate prediction vectors (batch, incaps_num, outcaps_num, outcaps_dim)

		pred_vectors = W.matmul(x).squeeze(-1)

		# calculate caps outputs (batch, outcaps_num, outcaps_dim)

		coupling_coeffs = Variable(tch.Tensor(x.size(0), self.incaps_num, self.outcaps_num).zero_())

		if tch.cuda.is_available():
			coupling_coeffs = coupling_coeffs.cuda()

		for r in range(routing_iters):

			if r == routing_iters - 1:
				output = self.__routing(pred_vectors, coupling_coeffs)
			else:
				output = self.__routing(pred_vectors.detach(), coupling_coeffs)

			# update coupling

				dot = (output.unsqueeze(1).expand(*pred_vectors.size()) * pred_vectors.detach()).sum(dim=-1)
				coupling_coeffs = coupling_coeffs + dot

		return output

class ConvCapsLayer(tch.nn.Module):

	def __init__(self, in_channels=None, kernel_size=None, stride=None, outcaps_num=None, outcaps_dim=None):

		super(ConvCapsLayer, self).__init__()

		self.in_channels = in_channels
		self.kernel_size = kernel_size
		self.stride = stride
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		self.convs = tch.nn.Conv2d(in_channels=in_channels, out_channels=outcaps_num*outcaps_dim, kernel_size=kernel_size, stride=stride)

	def forward(self, x):

		# output (batch, outcaps_num, outcaps_dim)

		o = self.convs(x).reshape(x.size(0), -1, self.outcaps_dim)

		return squash(o, dim=-1)

class CapsNet(tch.nn.Module):

	def __init__(self):

		super(CapsNet, self).__init__()

		self.conv = tch.nn.Sequential(tch.nn.Conv2d(3, 256, kernel_size=9),
				tch.nn.ReLU())

		self.primcaps = ConvCapsLayer(256, kernel_size=9, stride=2, outcaps_num=32, outcaps_dim=8)

		self.digitcaps = FCCapsLayer(incaps_num=32*8*4, incaps_dim=8, outcaps_num=10, outcaps_dim=16)

		self.reconst = tch.nn.Sequential(tch.nn.Linear(10*16, 512),
									tch.nn.ReLU(),
									tch.nn.Linear(512, 1024),
									tch.nn.ReLU(),
									tch.nn.Linear(1024, 3*23*32),
									tch.nn.Sigmoid())

	def forward(self, x, y=None):

		o = self.conv(x)
		primcaps = self.primcaps(o)
		o = self.digitcaps(primcaps)

		if y is not None:

			mask = tch.zeros_like(o).to(device)
			mask = mask.scatter(1, y.unsqueeze(-1).unsqueeze(-1).expand(-1, -1, o.size(-1)), 1.0)

			reconst = (mask*o).reshape(o.size(0), -1)
			reconst = self.reconst(reconst)

			return o, primcaps, reconst

		return o, primcaps, None

class CapsLoss(tch.nn.Module):

	def __init__(self):

		super(CapsLoss, self).__init__()

	def forward(self, o, y, reconst, x):

		o = o.norm(dim=-1)

		mask = tch.Tensor(o.size()).zero_()

		for i, target in enumerate(y):
			mask[i, target.cpu().data.numpy()] = 1

		mask = Variable(mask)

		m_plus = 0.9
		m_minus = 0.1

		if tch.cuda.is_available():
			mask = mask.cuda()

		class_loss = (mask*F.relu(m_plus - o)**2 + .5*(1-mask)*F.relu(o - m_minus)**2).sum(dim=-1).mean()
		reconst_loss = tch.nn.MSELoss()(reconst.reshape(-1, 3, 23, 32), x)

		return class_loss + 0.0005*3*23*32*reconst_loss

