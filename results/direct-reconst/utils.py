import torch as tch
import torchvision.transforms as transforms

class AverageMeter(object):

  def __init__(self):
    self.reset()

  def reset(self):
    self.avg = 0
    self.sum = 0
    self.cnt = 0

  def update(self, val, n=1):
    self.sum += val * n
    self.cnt += n
    self.avg = self.sum / self.cnt

def resize(x):
	x = tch.cat([transforms.ToTensor()(transforms.Resize(size=(4, 4))(transforms.ToPILImage()(img))) for img in x.cpu()], dim=0)

	if tch.cuda.is_available():
			return x.cuda()

	return x
