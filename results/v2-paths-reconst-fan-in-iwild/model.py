import math

import torch as tch
from torch.nn import init
import torch.nn.functional as F

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

PATH_CONV_CHANNELS = [16, 16, 16, 8]
PATH_LAYERS_NUM = 6
KERNEL_SIZE = 9
PADDING = 4
DIGIT_CAPS_NUM = 10
MAX_POOL_KERNEL_SIZE = 2

routing_iters = 3

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

class FCCapsLayer(tch.nn.Module):

	def __init__(self, incaps_num=None, incaps_dim=None, outcaps_num=None, outcaps_dim=None):

		super(FCCapsLayer, self).__init__()

		self.incaps_num = incaps_num
		self.incaps_dim = incaps_dim
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		# transformation matrices (incaps_num, outcaps_num, incaps_dim, outcaps_dim)

		stdv = 1. / math.sqrt(incaps_num)

		self.trans_matrices = tch.nn.Parameter(tch.empty(incaps_num, outcaps_num, incaps_dim, outcaps_dim).uniform_(-stdv, stdv))

	def __routing(self, pred_vectors, coupling_coeffs):

		norm_coupling_coeffs = F.softmax(coupling_coeffs, dim=1)

		norm_coupling_coeffs = norm_coupling_coeffs.unsqueeze(-1).expand_as(pred_vectors)

		output = norm_coupling_coeffs.mul(pred_vectors).sum(dim=1)

		return squash(output, dim=-1)

	def forward(self, x):

		# x is (batch, incaps_num, incaps_dim)

		# x is (batch, incaps_num, outcaps_num, incaps_dim, 1)
		x = x.unsqueeze(2).unsqueeze(-1).expand(-1, -1, self.outcaps_num, -1, -1)

		# W is (batch, incaps_num, outcaps_num, outcaps_dim, incaps_dim)
		W = self.trans_matrices.unsqueeze(0).expand(x.size(0), -1, -1, -1, -1).transpose(-1,-2)

		# calculate prediction vectors (batch, incaps_num, outcaps_num, outcaps_dim)
		pred_vectors = W.matmul(x).squeeze(-1)

		# calculate caps outputs (batch, outcaps_num, outcaps_dim)

		# coupling coeff is (batch, incaps_num, outcaps_num)
		coupling_coeffs = tch.zeros(x.size(0), self.incaps_num, self.outcaps_num).to(device)

		for r in range(routing_iters):

			if r == routing_iters - 1:
				output = self.__routing(pred_vectors, coupling_coeffs)
			else:
				output = self.__routing(pred_vectors.detach(), coupling_coeffs)

				# update coupling

				dot = output.unsqueeze(1).expand_as(pred_vectors).mul(pred_vectors.detach()).sum(dim=-1)
				coupling_coeffs = coupling_coeffs + dot

		return output

class ParallelConv2d(tch.nn.Module):

	def __init__(self, in_channels, out_channels, paths_num, kernel_size, padding):

		super(ParallelConv2d, self).__init__()

		self.out_channels = out_channels
		self.in_channels = in_channels
		self.paths_num = paths_num
		self.padding = padding

		self.weight = tch.nn.Parameter(tch.empty(paths_num*out_channels, in_channels, kernel_size, kernel_size))
		self.bias = tch.nn.Parameter(tch.empty(paths_num*out_channels))

		self.reset_parameters()

	def reset_parameters(self):
		init.kaiming_uniform_(self.weight, a=math.sqrt(5))
		if self.bias is not None:
			fan_in, _ = init._calculate_fan_in_and_fan_out(self.weight)
			bound = 1 / math.sqrt(fan_in)
			init.uniform_(self.bias, -bound, bound)

	def forward(self, x):

		# x: (batch, paths_num, in_channels, h, w)

		# x: (batch, paths_num*in_channels, h, w)
		x = x.reshape(x.size(0), -1, x.size(3), x.size(4))

		# o: (batch, paths_num*out_channels, h, w)
		o = F.conv2d(x, self.weight, bias=self.bias, padding=self.padding, groups=self.paths_num)

		return o.reshape(o.size(0), self.paths_num, self.out_channels, o.size(2), o.size(3))

class PathCapsNet(tch.nn.Module):
	'''
		@paths_dims:
			List of tuples. Each tuple is (kernel_size, path_width, path_depth, max_pool_size)

	'''
	def __init__(self, in_channels, paths_num, pathcaps_dim, digitcaps_dim):

		super(PathCapsNet, self).__init__()

		self.in_channels = in_channels
		self.paths_num = paths_num
		self.pathcaps_dim = pathcaps_dim
		self.digitcaps_dim = digitcaps_dim

		self.conv = tch.nn.ModuleList()

		_in_channels = in_channels
		for path_conv_idx, path_channels in enumerate(PATH_CONV_CHANNELS):

			self.conv.append(ParallelConv2d(_in_channels, path_channels, paths_num, KERNEL_SIZE, PADDING))
			_in_channels = path_channels

		self.digitcaps = FCCapsLayer(incaps_num=8*5*paths_num, incaps_dim=pathcaps_dim, outcaps_num=DIGIT_CAPS_NUM, outcaps_dim=digitcaps_dim)

		self.reconst = tch.nn.Sequential(tch.nn.Linear(10*16, 512),
									tch.nn.ReLU(),
									tch.nn.Linear(512, 1024),
									tch.nn.ReLU(),
									tch.nn.Linear(1024, 1*23*32),
									tch.nn.Sigmoid())

	def forward(self, x, y=None):

		o = x.unsqueeze(1).expand(-1, self.paths_num, -1, -1, -1)

		for conv_idx, conv in enumerate(self.conv):

			o = conv(o)
			o = F.relu(o)

			if (conv_idx+1) % 2 == 0:

				# _o: (batch, paths_num*channels, h, w)
				_o = o.reshape(o.size(0), -1, o.size(3), o.size(4))
				_o = F.max_pool2d(_o, kernel_size=MAX_POOL_KERNEL_SIZE)

				_o = _o.reshape(o.size(0), o.size(1), o.size(2), -1)
				h_size = int((o.size(3) - MAX_POOL_KERNEL_SIZE)/2.0 + 1.0)
				w_size = int((o.size(4) - MAX_POOL_KERNEL_SIZE)/2.0 + 1.0)
				o = _o.reshape(o.size(0), o.size(1), o.size(2), h_size, w_size)

		# o: (batch, incaps_num, incaps_dim)
		o = o.reshape(o.size(0), 8*5*self.paths_num, self.pathcaps_dim)

		primcaps = squash(o, -1)

		# o: (batch, outcaps_num, outcaps_dim)
		o = self.digitcaps(primcaps)

		if y is not None:

			mask = tch.zeros_like(o).to(device)
			mask = mask.scatter(1, y.unsqueeze(-1).unsqueeze(-1).expand(-1, -1, o.size(-1)), 1.0)

			reconst = (mask*o).reshape(o.size(0), -1)
			reconst = self.reconst(reconst)

			return o, primcaps, reconst

		return o, primcaps, None

class CapsLoss(tch.nn.Module):

	def __init__(self):

		super(CapsLoss, self).__init__()

	def forward(self, o, y, reconst, x):

		o = o.norm(dim=-1)

		mask = tch.zeros_like(o).to(device)
		mask = mask.scatter(1, y.unsqueeze(-1), 1.0)

		m_plus = 0.9
		m_minus = 0.1

		class_loss = (mask*F.relu(m_plus - o)**2 + .5*(1-mask)*F.relu(o - m_minus)**2).sum(dim=-1).mean()
		reconst_loss = tch.nn.MSELoss()(reconst.reshape(-1, 1, 23, 32), x)

		return class_loss + 0.0005*23*32*reconst_loss
