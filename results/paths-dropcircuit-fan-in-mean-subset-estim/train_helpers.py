import os
import logging as log

from logger import MainLogger as disklog

import torch as tch
import utils

def save_chkpnt(e, model, optim, path):
	tch.save({'e': e, 'model': model.state_dict(), 'optim': optim.state_dict()}, os.path.join(path,"chkpnt.pt"))

def load_chkpnt(path):

	path = os.path.join(path, 'chkpnt.pt')

	if os.path.exists(path):
		return tch.load(path)

	return None

def train_epoch(model, loss_fn, perform_fn, optim, train_ds):

	loss_avg = utils.AverageMeter()
	perform_avg = utils.AverageMeter()

	batches_count = len(train_ds)

	for b, (x, y) in enumerate(train_ds):

		model.train()

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o, _ = model(x, None)

		loss = loss_fn(o, y)
		perform = perform_fn(o, y)

		optim.zero_grad()
		loss.backward()
		optim.step()

		loss_avg.update(loss.item(), x.size(0))
		perform_avg.update(perform.item(), x.size(0))

		log.info("batch: {}/{} -- train_loss: {}".format(b+1, batches_count, loss_avg.avg))

	return loss_avg.avg, perform_avg.avg

def infer(model, perform_fn, valid_ds, test_ds):

	model.eval()

	coupling_coeffs = tch.FloatTensor(245, 10).zero_()

	if tch.cuda.is_available():
		coupling_coeffs = coupling_coeffs.cuda()

	batches_count = len(valid_ds)
	total_examples = 0.0

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():

			x = x.cuda()
			y = y.cuda()

		o, updated_coupling = model(x, None)

		coupling_coeffs = coupling_coeffs + updated_coupling.sum(dim=0)
		total_examples += x.size(0)

		log.info('Est coupling - batch: {}/{}'.format(b+1, batches_count))

	coupling_coeffs = coupling_coeffs.unsqueeze(0) / float(total_examples)

	loss_avg = utils.AverageMeter()

	batches_count = len(test_ds)

	for b, (x, y) in enumerate(test_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o, _ = model(x, coupling_coeffs)

		loss = perform_fn(o, y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	return loss_avg.avg

def train(model, loss_fn, perform_fn, optim_fn, train_ds, valid_ds, test_ds, epochs, results_path):

	optim = optim_fn(model.parameters())

	chkpnt = load_chkpnt(results_path)

	start_e = 0

	if chkpnt is not None:

		model.load_state_dict(chkpnt['model'])
		optim.load_state_dict(chkpnt['optim'])

		start_e = chkpnt['e'] + 1

	best_valid_err = None

	for e in range(start_e, epochs):

		log.info("epoch: {}/{}".format(e+1, epochs))

		train_err, train_perform = train_epoch(model, loss_fn, perform_fn, optim, train_ds)
		valid_err = infer(model, perform_fn, valid_ds, test_ds)

		if best_valid_err is None or valid_err < best_valid_err:

			best_valid_err = valid_err
			disklog.save_model(model)

		disklog.log_train_loss(e, train_err)
		disklog.log_train_perform(e, train_perform, valid_err)

		save_chkpnt(e, model, optim, results_path)

