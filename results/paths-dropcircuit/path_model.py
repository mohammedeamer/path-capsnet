import torch as tch
from torch.autograd import Variable
import torch.nn.functional as F

routing_iters = 3

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

class FCCapsLayer(tch.nn.Module):

	def __init__(self, incaps_num=None, incaps_dim=None, outcaps_num=None, outcaps_dim=None):

		super(FCCapsLayer, self).__init__()

		self.incaps_num = incaps_num
		self.incaps_dim = incaps_dim
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		# transformation matrices (incaps_num, outcaps_num, incaps_dim, outcaps_dim)

		self.trans_matrices = tch.nn.Parameter(tch.Tensor(incaps_num, outcaps_num, incaps_dim, outcaps_dim).normal_(), requires_grad=True)

		# coupling coeff is (incaps_num, outcaps_num)
		self.coupling_coeffs = Variable(tch.Tensor(self.incaps_num, self.outcaps_num).zero_())

		if tch.cuda.is_available():
			self.coupling_coeffs = self.coupling_coeffs.cuda()

	def __routing(self, pred_vectors, coupling_coeffs):

		norm_coupling_coeffs = F.softmax(coupling_coeffs, dim=0)

		norm_coupling_coeffs = norm_coupling_coeffs.unsqueeze(0).unsqueeze(-1).expand(*pred_vectors.size())

		output = (norm_coupling_coeffs * pred_vectors).sum(dim=1)

		return squash(output, dim=-1)

	def forward(self, x):

		# x is (batch, incaps_num, incaps_dim)

		# x is (batch, incaps_num, outcaps_num, incaps_dim, 1)
		x = x.unsqueeze(2).unsqueeze(-1).expand(-1, -1, self.outcaps_num, -1, -1)

		# W is (batch, incaps_num, outcaps_num, outcaps_dim, incaps_dim)
		W = self.trans_matrices.expand(x.size()[0], *self.trans_matrices.size()).transpose(-1,-2)

		# calculate prediction vectors (batch, incaps_num, outcaps_num, outcaps_dim)

		pred_vectors = W.matmul(x).squeeze(-1)

		# calculate caps outputs (batch, outcaps_num, outcaps_dim)

		coupling_coeffs = self.coupling_coeffs

		for r in range(routing_iters):

			if r == routing_iters - 1:
				output = self.__routing(pred_vectors, coupling_coeffs)
			else:
				output = self.__routing(pred_vectors.detach(), coupling_coeffs)

			# update coupling

			if r != routing_iters - 1:

				dot = (output.unsqueeze(1).expand(*pred_vectors.size()) * pred_vectors.detach()).sum(dim=-1).mean(dim=0)
				coupling_coeffs = coupling_coeffs + dot

		return output


class CNN(tch.nn.Module):

	'''
		@kerns_chs:
			List of tuples. Each tuple is (kernel_size, out_channels, max_pool_size)

	'''
	def __init__(self, in_chs, kerns_chs):

		super(CNN, self).__init__()

		self.layers_modules = list()

		prev_chs = in_chs

		for k, chs, max_pool in kerns_chs:

			mods = [tch.nn.Conv2d(prev_chs, chs, k, padding=4), tch.nn.ReLU()]

			if max_pool is not None:
				mods.append(tch.nn.MaxPool2d(max_pool))

			self.layers_modules.append(tch.nn.Sequential(*mods))

			prev_chs = chs

		self.layers_modules = tch.nn.ModuleList(self.layers_modules)

	def forward(self, x):

		o = x

		for m in self.layers_modules:
			o = m(o)

		return o

class PathCapsNet(tch.nn.Module):
	'''
		@paths_dims:
			List of tuples. Each tuple is (kernel_size, path_width, path_depth, max_pool_size)

	'''
	def __init__(self, in_chs, paths_dims, pathcaps_dim, digitcaps_dim, dropcirc):

		super(PathCapsNet, self).__init__()

		self.in_chs = in_chs
		self.digitcaps_dim = digitcaps_dim
		self.pathcaps_dim = pathcaps_dim
		self.dropcirc = dropcirc

		self.paths = list()

		for p_dims in paths_dims:

			depth = p_dims[2]

			assert depth > 1

			mp_idx = int(depth/2)
			kerns_chs = [[p_dims[0], p_dims[1], None] for _ in range(depth-1)]
			kerns_chs.append([p_dims[0], pathcaps_dim, None])
			kerns_chs[mp_idx][2] = p_dims[3]
			kerns_chs[-1][2] = p_dims[3]

			kerns_chs = map(tuple, kerns_chs)

			self.paths.append(CNN(in_chs=in_chs, kerns_chs=kerns_chs))

		self.paths = tch.nn.ModuleList(self.paths)
		self.digitcaps = FCCapsLayer(incaps_num=7*7*len(self.paths), incaps_dim=pathcaps_dim, outcaps_num=10, outcaps_dim=digitcaps_dim)

	def __calc_paths_output(self, x):

		paths_outputs = list()

		mask = tch.ones(len(self.paths))

		if self.training:
			mask.bernoulli_(self.dropcirc)

		if tch.cuda.is_available():
			mask = mask.cuda()

		for p, path in enumerate(self.paths):
			paths_outputs.append(mask[p]*path(x).view(x.size(0), -1, self.pathcaps_dim))

		o = squash(tch.cat(paths_outputs, 1), -1)
		return self.digitcaps(o)

	def forward(self, x):
		return self.__calc_paths_output(x)

class CapsLoss(tch.nn.Module):

	def __init__(self):

		super(CapsLoss, self).__init__()

	def forward(self, o, y):

		o = o.norm(dim=-1)

		mask = tch.Tensor(o.size()).zero_()

		for i, target in enumerate(y):
			mask[i, target.cpu().data.numpy()] = 1

		mask = Variable(mask)

		m_plus = 0.9
		m_minus = 0.1

		if tch.cuda.is_available():
			mask = mask.cuda()

		return (mask*F.relu(m_plus - o)**2 + .5*(1-mask)*F.relu(o - m_minus)**2).sum(dim=-1).mean()

