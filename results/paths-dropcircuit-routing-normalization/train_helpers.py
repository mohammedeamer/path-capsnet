import logging as log

from logger import MainLogger as disklog

import torch as tch
import utils

def infer_with_normalization(model, perform_fn, train_ds, valid_ds):

	model.eval()

	coupling_coeffs = tch.FloatTensor(245, 10).zero_()

	if tch.cuda.is_available():
		coupling_coeffs = coupling_coeffs.cuda()

	batches_count = len(train_ds)
	total_samples = 0.0

	for b, (x, y) in enumerate(train_ds):

		if tch.cuda.is_available():

			x = x.cuda()
			y = y.cuda()

		_, updated_coeffs = model(x)

		coupling_coeffs = coupling_coeffs + updated_coeffs.sum(dim=0)

		total_samples += x.size(0)

		log.info('Calc coupling coeffs -- batch: {}/{}'.format(b+1, batches_count))

	coupling_coeffs = coupling_coeffs / float(total_samples)
	coupling_coeffs = coupling_coeffs.unsqueeze(0)

	loss_avg = utils.AverageMeter()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o, _ = model(x, coupling_coeffs.expand(x.size(0), -1, -1))

		loss = perform_fn(o, y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	disklog.log_test_perform(loss_avg.avg)

	return loss_avg.avg

