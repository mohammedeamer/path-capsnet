import sys, os, argparse
import logging

import numpy as np
import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

from logger import MainLogger as disklog
import train_helpers as helpers
from model import CapsNet, CapsLoss

CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			}
}

NUM_WORKERS = 2

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/cifar-10/exp-data")

exp_results_path = os.path.join(dir_path, "exp-results")

if not os.path.exists(exp_results_path):
	os.makedirs(exp_results_path)

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

def predictions(o):
	return np.argmax(o.norm(dim=-1).cpu().data.numpy(), axis=-1)

def perform_fn(o, y):

	preds = predictions(o)
	return (preds != y.cpu().data.numpy()).sum() / y.size(0)

def train(tag):

	results_path = os.path.join(dir_path, "exp-results", tag)

	if not os.path.exists(results_path):
		os.makedirs(results_path)

	disklog.init(results_path)

	disklog.save_config(CONFIG)

	trials = CONFIG["exp_params"]["train_trials"]

	trans = transforms.Compose([transforms.ToTensor()])

	train_ds = dutils.DataLoader(dutils.Subset(dset.CIFAR10(root=ds_path, train=True, transform=trans, download=True),range(45000)), batch_size=128, shuffle=True, num_workers=NUM_WORKERS)

	valid_ds = dutils.DataLoader(dutils.Subset(dset.CIFAR10(root=ds_path, train=True, transform=trans, download=True), range(45000, 50000)), batch_size=256, shuffle=True, num_workers=NUM_WORKERS)

	test_ds = dutils.DataLoader(dset.CIFAR10(root=ds_path, train=False, transform=trans, download=True), shuffle=True, batch_size=256, num_workers=NUM_WORKERS)

	for t in range(1, trials+1):

		trial_results_path = os.path.join(results_path, str(t))

		if not os.path.exists(trial_results_path):
			os.makedirs(trial_results_path)

		disklog.init(trial_results_path)

		model = CapsNet()

		if tch.cuda.is_available():
			model.cuda()

		helpers.train(model, CapsLoss(), perform_fn=perform_fn, optim_fn=tch.optim.Adam, train_ds=train_ds, valid_ds=valid_ds, epochs=CONFIG["exp_params"]["train_epochs"], results_path=trial_results_path)

		model.load_state_dict(tch.load(os.path.join(trial_results_path, 'model.pt')))

		test_err = helpers.infer(model, perform_fn=perform_fn, valid_ds=test_ds)

		disklog.log_test_perform(test_err)

SUBEXPS = {
	"train": train,
}

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("subexp", action="store")
	parser.add_argument("tag", action="store")

	args = parser.parse_args()

	SUBEXPS[args.subexp](args.tag)
