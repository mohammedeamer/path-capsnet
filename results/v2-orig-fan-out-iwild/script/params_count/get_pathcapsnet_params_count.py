import sys, os, argparse, functools

import torch as tch

IN_CHANNELS = 1
PATHS_NUM = 16
PATHCAPS_DIM = 8
DIGITCAPS_DIM = 16

def run(exp_root, dropcirc):

	if dropcirc == 'True':
		dropcirc = True
	else:
		dropcirc = False

	sys.path.insert(0, exp_root)

	from model import PathCapsNet

	model = functools.partial(PathCapsNet, in_channels=IN_CHANNELS, paths_num=PATHS_NUM, pathcaps_dim=PATHCAPS_DIM, digitcaps_dim=DIGITCAPS_DIM)

	if dropcirc:
		model = model(drop_prob=0.5)
	else:
		model = model()

	params_count = tch.cat([p.view(-1) for p in model.parameters()], dim=0).size(0)

	exp_name = os.path.basename(exp_root)

	print('{} - params_count: {}'.format(exp_name, params_count))


if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	parser.add_argument('exp_root', action='store')
	parser.add_argument('dropcirc', action='store')

	args = parser.parse_args()

	run(args.exp_root, args.dropcirc)

