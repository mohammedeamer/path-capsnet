#!/bin/bash

EXPS_ROOT=$1

for d in $(ls $EXPS_ROOT)
do

        if [[ -d $EXPS_ROOT/$d ]] && [[ $d == *iwild ]] && [[ $d == *paths* ]]
        then
                echo $d;

                if [[ $d == *dropcircuit* ]]
                then
                        DROPCIRC=True
                else
                        DROPCIRC=False
                fi

                python get_pathcapsnet_params_count.py $EXPS_ROOT/$d $DROPCIRC
        fi
done;



