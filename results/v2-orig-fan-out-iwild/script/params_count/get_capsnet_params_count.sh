#!/bin/bash

EXPS_ROOT=$1

for d in $(ls $EXPS_ROOT)
do

        if [[ -d $EXPS_ROOT/$d ]] && [[ $d == *iwild ]] && [[ $d == *orig* ]]
        then
                echo $d;
                python get_capsnet_params_count.py $EXPS_ROOT/$d
        fi
done;



