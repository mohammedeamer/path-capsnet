import logging as log

from logger import MainLogger as disklog

import torch as tch
import utils

def infer_with_normal_noise(model, perform_fn, valid_ds):

	model.eval()

	loss_avg = utils.AverageMeter()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		noise = tch.FloatTensor(3, 1, 28, 28).normal_()

		if tch.cuda.is_available():
			noise = noise.cuda()

		aug_x = tch.cat([x, (x.expand_as(noise)+noise).clamp(min=0.0, max=1.0)], dim=0)

		o, _ = model(aug_x)

		loss = perform_fn(o[0], y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	disklog.log_test_perform(loss_avg.avg)

	return loss_avg.avg

