import sys, os, argparse
import logging

import numpy as np
import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

from logger import MainLogger as disklog
import train_helpers as helpers
from path_model import PathCapsNet

CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3
			}
}

NUM_WORKERS = 2

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/mnist/exp-data")

exp_results_path = os.path.join(dir_path, "exp-results")

if not os.path.exists(exp_results_path):
	os.makedirs(exp_results_path)

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

def predictions(o):
	return np.argmax(o.norm(dim=-1).cpu().data.numpy(), axis=-1)

def perform_fn(o, y):

	preds = predictions(o)
	return (preds != y.cpu().data.numpy()).sum() / y.size(0)

def test(tag, task_id, trial):

	trial_results_path = os.path.join(dir_path, "exp-results", tag, str(task_id), str(trial))

	infer_results_path = os.path.join(trial_results_path, 'infer')

	os.makedirs(infer_results_path)

	disklog.init(infer_results_path)

	test_ds = dutils.DataLoader(dset.MNIST(root=ds_path, train=False, transform=transforms.Compose([transforms.ToTensor()]), download=True), batch_size=1, num_workers=NUM_WORKERS)

	model = PathCapsNet(1, paths_dims=[(9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2)], pathcaps_dim=8, digitcaps_dim=16, dropcirc=0.5)

	if tch.cuda.is_available():
		model.cuda()

	model.load_state_dict(tch.load(os.path.join(trial_results_path, 'model.pt')))

	helpers.infer_with_normal_noise(model, perform_fn=perform_fn, valid_ds=test_ds)

SUBEXPS = {
	"infer": test,
}

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("subexp", action="store")
	parser.add_argument("tag", action="store")
	parser.add_argument("task_id", action="store")
	parser.add_argument("trial", action="store")

	args = parser.parse_args()

	SUBEXPS[args.subexp](args.tag, args.task_id, args.trial)
