import os

import numpy as np
import pandas as pd
import torch as tch
from torch.utils.data import TensorDataset, DataLoader

class CSVDataset(TensorDataset):

	def __init__(self, path):

		self.data = pd.read_csv(path)

	def __len__(self):
		return self.data.shape[0]

	def __getitem__(self, idx):

		row = self.data.iloc[idx]
		return int(row.iloc[0]), tch.from_numpy(np.array(row.iloc[1:-1])).type(tch.FloatTensor).view(28, 28).unsqueeze(0), int(row.iloc[-1])

def train_source(ds_path):
	return DataLoader(CSVDataset(os.path.join(ds_path, "train.csv")), batch_size=128, num_workers=1)

def test_source(ds_path):
	return DataLoader(CSVDataset(os.path.join(ds_path, "test.csv")), batch_size=256, num_workers=1)
