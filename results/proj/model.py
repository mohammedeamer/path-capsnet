import torch as tch
from torch.autograd import Variable
import torch.nn.functional as F

routing_iters = 3

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

class FCCapsLayer(tch.nn.Module):

	def __init__(self, incaps_num=None, incaps_dim=None, outcaps_num=None, outcaps_dim=None):

		super(FCCapsLayer, self).__init__()

		self.incaps_num = incaps_num
		self.incaps_dim = incaps_dim
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		self.left_mat = tch.nn.Parameter(tch.Tensor(outcaps_num, incaps_num).normal_(), requires_grad=True)
		self.right_mat = tch.nn.Parameter(tch.Tensor(incaps_dim, outcaps_dim).normal_(), requires_grad=True)

		self.b1 = tch.nn.Parameter(tch.Tensor(outcaps_dim).normal_(), requires_grad=True)
		self.b2 = tch.nn.Parameter(tch.Tensor(outcaps_dim).normal_(), requires_grad=True)

	def forward(self, x):

		# x is (batch, incaps_num, incaps_dim)

		return squash(self.left_mat @ squash(x @ self.right_mat + self.b1, dim=-1) + self.b2, dim=-1)

class ConvCapsLayer(tch.nn.Module):

	def __init__(self, in_channels=None, kernel_size=None, stride=None, outcaps_num=None, outcaps_dim=None):

		super(ConvCapsLayer, self).__init__()

		self.in_channels = in_channels
		self.kernel_size = kernel_size
		self.stride = stride
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		self.convs = [tch.nn.Conv2d(in_channels=in_channels, out_channels=outcaps_dim, kernel_size=kernel_size, stride=stride)
						for _ in range(outcaps_num)]

		self.convs = tch.nn.ModuleList(self.convs)

	def forward(self, x):

		# output (batch, outcaps_num, outcaps_dim)

		output = [conv(x).view(x.size()[0], -1, self.outcaps_dim) for conv in self.convs]

		return squash(tch.cat(output, dim=1), dim=-1)

class CapsLoss(tch.nn.Module):

	def __init__(self):

		super(CapsLoss, self).__init__()

	def forward(self, o, y):

		o = o.norm(dim=-1)

		mask = tch.Tensor(o.size()).zero_()

		for i, target in enumerate(y):
			mask[i, target.cpu().data.numpy()] = 1

		mask = Variable(mask)

		m_plus = 0.9
		m_minus = 0.1

		if tch.cuda.is_available():
			mask = mask.cuda()

		return (mask*F.relu(m_plus - o)**2 + .5*(1-mask)*F.relu(o - m_minus)**2).sum(dim=-1).mean()

