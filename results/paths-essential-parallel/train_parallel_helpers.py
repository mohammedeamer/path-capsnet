import os
import logging as log

from logger import MainLogger as disklog

import torch as tch
import torch.distributed as dist

import utils

def average_gradients(model):
	size = float(dist.get_world_size())
	for param in model.parameters():
		dist.all_reduce(param.grad.data, op=dist.reduce_op.SUM)
		param.grad.data /= size

def save_chkpnt(e, model, optim, path):
	tch.save({'e': e, 'model': model.state_dict(), 'optim': optim.state_dict()}, os.path.join(path,"chkpnt.pt"))

def load_chkpnt(path):

	path = os.path.join(path, 'chkpnt.pt')

	if os.path.exists(path):
		return tch.load(path)

	return None

def train_epoch(model, loss_fn, perform_fn, optim, train_ds):

	loss_avg = utils.AverageMeter()
	perform_avg = utils.AverageMeter()

	batches_count = len(train_ds)

	for b, (x, y) in enumerate(train_ds):

		model.train()

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o = model(x)

		loss = loss_fn(o, y)
		perform = perform_fn(o, y)

		optim.zero_grad()
		loss.backward()
		average_gradients(model)
		optim.step()

		loss_avg.update(loss.item(), x.size(0))
		perform_avg.update(perform.item(), x.size(0))

		log.info("batch: {}/{} -- train_loss: {}".format(b+1, batches_count, loss_avg.avg))

	return loss_avg.avg, perform_avg.avg

def infer(model, perform_fn, valid_ds):

	loss_avg = utils.AverageMeter()

	model.eval()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		o = model(x)

		loss = perform_fn(o, y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	return loss_avg.avg

def train(model, loss_fn, perform_fn, optim_fn, train_ds, valid_ds, epochs, results_path, rank):

	optim = optim_fn(model.parameters())

	chkpnt = load_chkpnt(results_path)

	start_e = 0

	if chkpnt is not None:

		model.load_state_dict(chkpnt['model'])
		optim.load_state_dict(chkpnt['optim'])

		start_e = chkpnt['e'] + 1

	best_valid_err = None

	for e in range(start_e, epochs):

		log.info("epoch: {}/{}".format(e+1, epochs))

		train_err, train_perform = train_epoch(model, loss_fn, perform_fn, optim, train_ds)
		train_err, train_perform = tch.tensor(train_err), tch.tensor(train_perform)
		valid_err = tch.tensor(infer(model, perform_fn, valid_ds))

		world_size = dist.get_world_size()

		dist.reduce(train_err, 0, op=dist.reduce_op.SUM)
		dist.reduce(train_perform, 0, op=dist.reduce_op.SUM)
		dist.reduce(valid_err, 0, op=dist.reduce_op.SUM)

		train_err = train_err.item()/world_size
		train_perform = train_perform.item()/world_size
		valid_err = valid_err.item()/world_size

		if (rank == 0):

			if best_valid_err is None or valid_err < best_valid_err:

				best_valid_err = valid_err
				disklog.save_model(model)

			disklog.log_train_loss(e, train_err)
			disklog.log_train_perform(e, train_perform, valid_err)

			save_chkpnt(e, model, optim, results_path)

