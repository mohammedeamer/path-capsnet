import sys, os, argparse
import logging

import numpy as np
import torch as tch
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torch.distributed as dist

from logger import MainLogger as disklog
import train_parallel_helpers as helpers
from path_model import PathCapsNet, CapsLoss
from datasource import partition_dataset

CONFIG = {
	"exp_params": {
				"train_epochs": 200,
				'world_size': 16
			}
}

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/mnist/exp-data")

exp_results_path = os.path.join(dir_path, "exp-results")

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

def predictions(o):
	return np.argmax(o.norm(dim=-1).cpu().data.numpy(), axis=-1)

def perform_fn(o, y):

	preds = predictions(o)
	return (preds != y.cpu().data.numpy()).sum() / y.size(0)

def train(tag, rank):

	rank = int(rank)

	results_path = os.path.join(dir_path, "exp-results", tag)

	if rank == 0:

		if not os.path.exists(exp_results_path):
			os.makedirs(exp_results_path)

		if not os.path.exists(results_path):
			os.makedirs(results_path)

		disklog.init(results_path)

		disklog.save_config(CONFIG)

		fh = logging.FileHandler(os.path.join(results_path, 'master_log.txt'))
		fh.setFormatter(logging.Formatter(log_format))
		logging.getLogger().addHandler(fh)

	dist.init_process_group("tcp", init_method="file://{}".format(os.path.join(results_path, "shared_file")), rank=rank, world_size=CONFIG['exp_params']['world_size'])

	trans = transforms.Compose([transforms.Pad(2), transforms.RandomCrop(28), transforms.ToTensor()])

	train_ds = partition_dataset(dset.MNIST(root=ds_path, train=True, transform=trans, download=False), rank, bsz=128, seed=None, shuffle=True, drop_last=False)[0]

	test_ds = partition_dataset(dset.MNIST(root=ds_path, train=False, transform=transforms.Compose([transforms.ToTensor()]), download=False), rank, bsz=256, seed=None, shuffle=False, drop_last=False)[0]

	model = PathCapsNet(1, paths_dims=[(9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2), (9, 16, 5, 2)], pathcaps_dim=8, digitcaps_dim=16)

	if tch.cuda.is_available():
		model.cuda()

	helpers.train(model, CapsLoss(), perform_fn=perform_fn, optim_fn=tch.optim.Adam, train_ds=train_ds, valid_ds=test_ds, epochs=CONFIG["exp_params"]["train_epochs"], results_path=results_path, rank=rank)

SUBEXPS = {
	"train": train,
}

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("subexp", action="store")
	parser.add_argument("tag", action="store")
	parser.add_argument("rank", action="store")

	args = parser.parse_args()

	SUBEXPS[args.subexp](args.tag, args.rank)
