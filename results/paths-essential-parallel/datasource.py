from random import Random

from torch.utils.data import DataLoader
import torch.distributed as dist

class Partition(object):

  def __init__(self, data, index):
    self.data = data
    self.index = index

  def __len__(self):
    return len(self.index)

  def __getitem__(self, index):
    data_idx = self.index[index]
    return self.data[data_idx]

class DataPartitioner(object):

	def __init__(self, data, sizes, seed=None):
		self.data = data
		self.partitions = []

		data_len = len(data)
		indexes = [x for x in range(0, data_len)]

		if seed is not None:
			rng = Random()
			rng.seed(seed)
			rng.shuffle(indexes)

		for frac in sizes:
			part_len = int(frac * data_len)
			self.partitions.append(indexes[0:part_len])
			indexes = indexes[part_len:]

	def use(self, partition):
		return Partition(self.data, self.partitions[partition])

def partition_dataset(dataset, rank, bsz=128, seed=None, shuffle=False, drop_last=False):

	size = dist.get_world_size()
	bsz = int(bsz / float(size))
	partition_sizes = [1.0 / size for _ in range(size)]
	partition = DataPartitioner(dataset, partition_sizes, seed)
	partition = partition.use(rank)
	train_set = DataLoader(partition, batch_size=bsz, shuffle=shuffle, drop_last=drop_last)

	return train_set, bsz
