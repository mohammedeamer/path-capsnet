import os, sys, argparse, functools, logging

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

from rdm_utils import calc_rdm_corr, log_corr
from comb_dataset import CombDataset

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

NUM_WORKERS = 2

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/mnist/exp-data")

def run(exp_root, config_path, model_path, result_path, is_drop, paths_num, subset_size):

	sys.path.insert(0, exp_root)

	from path_model import PathCapsNet

	model = functools.partial(PathCapsNet, 1, paths_dims=[(9, 16, 5, 2) for _ in range(paths_num)], pathcaps_dim=8, digitcaps_dim=16)

	if is_drop == 'True':
		model = model(dropcirc=0.5)
	else:
		model = model()

	model.to(device)
	model.load_state_dict(tch.load(model_path, map_location=device))

	test_ds = dutils.Subset(dset.MNIST(root=ds_path, train=False, transform=transforms.Compose([transforms.ToTensor()]), download=True), range(subset_size))

	test_ds = dutils.DataLoader(CombDataset(test_ds), shuffle=True, batch_size=256, num_workers=NUM_WORKERS)

	corr = calc_rdm_corr(model, 'pathcapsnet', paths_num, test_ds)

	log_corr(result_path, corr)

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument('exp_root', action='store')
	parser.add_argument('config_path', action='store')
	parser.add_argument('model_path', action='store')
	parser.add_argument('result_path', action='store')
	parser.add_argument('is_drop', action='store')
	parser.add_argument('paths_num', action='store')
	parser.add_argument('subset_size', action='store')

	args = parser.parse_args()

	run(args.exp_root, args.config_path, args.model_path, args.result_path, args.is_drop, int(args.paths_num), int(args.subset_size))

