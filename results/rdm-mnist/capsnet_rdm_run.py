import os, sys, argparse, logging

import torch as tch
import torch.utils.data as dutils
import torchvision.datasets as dset
import torchvision.transforms as transforms

from rdm_utils import calc_rdm_corr, log_corr
from comb_dataset import CombDataset

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
    format=log_format, datefmt='%m/%d %I:%M:%S %p')

NUM_WORKERS = 2

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../data/mnist/exp-data")

def run(exp_root, model_path, result_path, subset_size):

	sys.path.insert(0, exp_root)

	try:

		from model import CapsNet

		model = CapsNet().to(device)
		model.load_state_dict(tch.load(model_path, map_location=device))

		model_type = 'capsnet'

	except:

		from model import FCCapsLayer, ConvCapsLayer

		model = tch.nn.Sequential(tch.nn.Conv2d(1, 256, kernel_size=9),
				tch.nn.ReLU(),
				ConvCapsLayer(256, kernel_size=9, stride=2, outcaps_num=32, outcaps_dim=8),
				FCCapsLayer(incaps_num=32*6*6, incaps_dim=8, outcaps_num=10, outcaps_dim=16)).to(device)

		model.load_state_dict(tch.load(model_path, map_location=device))

		model_type = 'capsnet-old'

	test_ds = dutils.Subset(dset.MNIST(root=ds_path, train=False, transform=transforms.Compose([transforms.ToTensor()]), download=True), range(subset_size))

	test_ds = dutils.DataLoader(CombDataset(test_ds), shuffle=True, batch_size=256, num_workers=NUM_WORKERS)

	corr = calc_rdm_corr(model, model_type, 32, test_ds)

	log_corr(result_path, corr)

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument('exp_root', action='store')
	parser.add_argument('model_path', action='store')
	parser.add_argument('result_path', action='store')
	parser.add_argument('subset_size', action='store')

	args = parser.parse_args()

	run(args.exp_root, args.model_path, args.result_path, int(args.subset_size))

