import torch as tch
from torch.autograd import Variable
import torch.nn.functional as F

routing_iters = 3

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

class FCCapsLayer(tch.nn.Module):

	def __init__(self, incaps_num=None, incaps_dim=None, outcaps_num=None, outcaps_dim=None):

		super(FCCapsLayer, self).__init__()

		self.incaps_num = incaps_num
		self.incaps_dim = incaps_dim
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		# transformation matrices (incaps_num, outcaps_num, incaps_dim, outcaps_dim)

		self.trans_matrices = tch.nn.Parameter(tch.Tensor(incaps_num, outcaps_num, incaps_dim, outcaps_dim).normal_(), requires_grad=True)

		# coupling coeff is (incaps_num, outcaps_num)
		self.coupling_coeffs = Variable(tch.Tensor(self.incaps_num, self.outcaps_num).zero_())

		if tch.cuda.is_available():
			self.coupling_coeffs = self.coupling_coeffs.cuda()

	def __routing(self, pred_vectors, coupling_coeffs):

		norm_coupling_coeffs = F.softmax(coupling_coeffs, dim=0)

		norm_coupling_coeffs = norm_coupling_coeffs.unsqueeze(0).unsqueeze(-1).expand(*pred_vectors.size())

		output = (norm_coupling_coeffs * pred_vectors).sum(dim=1)

		return squash(output, dim=-1)

	def forward(self, x):

		# x is (batch, incaps_num, incaps_dim)

		# x is (batch, incaps_num, outcaps_num, incaps_dim, 1)
		x = x.unsqueeze(2).unsqueeze(-1).expand(-1, -1, self.outcaps_num, -1, -1)

		# W is (batch, incaps_num, outcaps_num, outcaps_dim, incaps_dim)
		W = self.trans_matrices.expand(x.size()[0], *self.trans_matrices.size()).transpose(-1,-2)

		# calculate prediction vectors (batch, incaps_num, outcaps_num, outcaps_dim)

		pred_vectors = W.matmul(x).squeeze(-1)

		# calculate caps outputs (batch, outcaps_num, outcaps_dim)

		coupling_coeffs = self.coupling_coeffs

		for r in range(routing_iters):

			if r == routing_iters - 1:
				output = self.__routing(pred_vectors, coupling_coeffs)
			else:
				output = self.__routing(pred_vectors.detach(), coupling_coeffs)

			# update coupling

				dot = (output.unsqueeze(1).expand(*pred_vectors.size()) * pred_vectors.detach()).sum(dim=-1).mean(dim=0)
				coupling_coeffs = coupling_coeffs + dot

		return output

class ConvCapsLayer(tch.nn.Module):

	def __init__(self, in_channels=None, kernel_size=None, stride=None, outcaps_num=None, outcaps_dim=None):

		super(ConvCapsLayer, self).__init__()

		self.in_channels = in_channels
		self.kernel_size = kernel_size
		self.stride = stride
		self.outcaps_num = outcaps_num
		self.outcaps_dim = outcaps_dim

		self.convs = [tch.nn.Conv2d(in_channels=in_channels, out_channels=outcaps_dim, kernel_size=kernel_size, stride=stride)
						for _ in range(outcaps_num)]

		self.convs = tch.nn.ModuleList(self.convs)

	def forward(self, x):

		# output (batch, outcaps_num, outcaps_dim)

		output = [conv(x).view(x.size()[0], -1, self.outcaps_dim) for conv in self.convs]

		return squash(tch.cat(output, dim=1), dim=-1)

class CapsLoss(tch.nn.Module):

	def __init__(self):

		super(CapsLoss, self).__init__()

	def forward(self, o, y):

		o = o.norm(dim=-1)

		mask = tch.Tensor(o.size()).zero_()

		for i, target in enumerate(y):
			mask[i, target.cpu().data.numpy()] = 1

		mask = Variable(mask)

		m_plus = 0.9
		m_minus = 0.1

		if tch.cuda.is_available():
			mask = mask.cuda()

		return (mask*F.relu(m_plus - o)**2 + .5*(1-mask)*F.relu(o - m_minus)**2).sum(dim=-1).mean()

