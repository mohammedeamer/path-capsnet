import logging as log

from logger import MainLogger as disklog

import torch as tch
import torchvision.transforms as transforms
import utils

def infer_with_random_crop(model, perform_fn, valid_ds):

	model.eval()

	trans = transforms.Compose([transforms.ToPILImage(), transforms.Pad(2), transforms.RandomCrop(28), transforms.ToTensor()])

	loss_avg = utils.AverageMeter()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		crop_x = [trans(x) for _ in range(3)]

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

			crop_x = [x.cuda() for x in crop_x]

		y = y.unsqueeze(0)

		aug_x = [x]
		aug_x += crop_x

		aug_x = [x.unsqueeze(1) for x in aug_x]

		aug_x = tch.cat(aug_x, dim=0)

		o, _ = model(aug_x)

		loss = perform_fn(o[0], y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	disklog.log_test_perform(loss_avg.avg)

	return loss_avg.avg

