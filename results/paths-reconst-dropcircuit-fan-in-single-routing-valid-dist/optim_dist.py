import torch as tch
from torch.autograd import grad, backward
import torch.distributed as dist

class DistOptim(object):

	def __init__(self, model, optim, loss_fn, perform_fn):

		self.model = model
		self.optim = optim
		self.loss_fn = loss_fn
		self.perform_fn = perform_fn

		self.rank = dist.get_rank()

	def step(self, x, y):

		o = self.model(x, y)

		if self.rank == 0:

			o, inputs, reconst = o
			loss = self.loss_fn(o, y, reconst, x)

			self.optim.zero_grad()
			backward(loss, retain_graph=True)
			self.optim.step()

			grads = grad(loss, inputs)

			for i, g in enumerate(grads):
				dist.send(g.contiguous(), dst=i+1)

			return loss.item(), self.perform_fn(o, y).item()

		g = tch.zeros_like(o)
		dist.recv(g, src=0)

		self.optim.zero_grad()
		backward(o, grad_tensors=g)
		self.optim.step()

		return None, None

