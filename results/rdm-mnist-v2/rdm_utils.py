import os, itertools, csv, logging

import torch as tch
import torch.nn.functional as F

device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')

def calc_rdm_corr(model, model_type, primcaps_num, ds_loader):

	if model_type == 'capsnet':
		model_fn = get_capsnet_primcaps
	elif model_type == 'capsnet-old':
		model_fn = get_capsnet_old_primcaps
	else:
		model_fn = get_pathcapsnet_primcaps

	with tch.no_grad():

		model.eval()

		batches_count = len(ds_loader)

		# calc each primary capsule RDMs

		rdms = []

		for batch_idx, (x_1, x_2) in enumerate(ds_loader):

			logging.info('RDMs - batch: {}/{}'.format(batch_idx+1, batches_count))

			x_1 = x_1.to(device)
			x_2 = x_2.to(device)

			primcaps_1 = model_fn(model, x_1)
			primcaps_1 = primcaps_1.reshape(primcaps_1.size(0), primcaps_num, -1)

			primcaps_2 = model_fn(model, x_2).reshape_as(primcaps_1)

			# (samples, primcaps_num)
			_rdms = (primcaps_1 - primcaps_2).pow(2.0).sum(dim=-1).sqrt()

			rdms.append(_rdms)

		# (primcaps_num, samples)
		rdms = tch.cat(rdms, dim=0).transpose(0,1)

		# calc primcaps corr

		primcaps_num = rdms.size(0)

		primcaps_indices = range(primcaps_num)
		primcaps_indices = list(itertools.combinations(primcaps_indices, 2))

		caps_pairs_count = len(primcaps_indices)

		corrs = []

		for caps_pair_idx, (idx_1, idx_2) in enumerate(primcaps_indices):

			logging.info('Corr - pair: {}/{}'.format(caps_pair_idx+1, caps_pairs_count))

			primcaps_1 = rdms[idx_1]
			primcaps_2 = rdms[idx_2]

			mu_1 = primcaps_1.mean()
			mu_2 = primcaps_2.mean()

			cov = (primcaps_1 - mu_1).mul(primcaps_2 - mu_2).mean()

			sig_1 = primcaps_1.std()
			sig_2 = primcaps_2.std()

			corr = cov/(sig_1*sig_2)
			corrs.append([caps_pair_idx, corr.item()])

		return corrs

def log_corr(path, rows):

	with open(os.path.join(path), 'w') as f:
		csv_writer = csv.writer(f)
		csv_writer.writerows(rows)

def squash(x=None, dim=None):

	eps = 1e-9

	norm_sq = x.norm(dim=dim).unsqueeze(-1).expand(*x.size())**2

	return (norm_sq / (1 + norm_sq)) * (x / (norm_sq.sqrt() + eps))

def get_capsnet_primcaps(model, x):
	o = model.conv(x)
	o = F.relu(o)
	return model.primcaps(o)

def get_capsnet_old_primcaps(model, x):
	return model[0:3](x)

def get_pathcapsnet_primcaps(model, x):

	paths_outputs = list()

	for p, path in enumerate(model.paths):
		paths_outputs.append(path(x).view(x.size(0), -1, 8))

	return squash(tch.cat(paths_outputs, 1), -1)
