#!/bin/bash

set -e

EXPS=("paths-dropcircuit-fan-out-single-routing-valid/exp-results/no-reconst-[16-5]x5-dropcircuit-fan-out-single-routing-valid/1/" "orig-reconst-fan-in-single-routing-valid/exp-results/capsnet-reconst-fan-in-single-routing-valid/1/" "orig-fan-in-single-routing-valid/exp-results/capsnet-fan-in-single-routing-valid/1/" "paths-reconst-dropcircuit-fan-in-single-routing-valid/exp-results/pcn-reconst-[16-5]x10-dropcircuit-fan-in-single-routing-valid/1/"
"paths-reconst-dropcircuit-fan-in-single-routing-valid/exp-results/pcn-reconst-[16-5]x16-dropcircuit-fan-in-single-routing-valid/1/" "paths-reconst-fan-in-single-routing-valid/exp-results/paths-reconst-fan-in-single-routing-valid-[16-5]x10/1/" "paths-dropcircuit-fan-in-single-routing-valid/exp-results/no-reconst-[16-5]x5-dropcircuit-fan-in-single-routing-valid/1/"
"paths-dropcircuit-fan-in-single-routing-valid/exp-results/no-reconst-[16-5]x10-dropcircuit-fan-in-single-routing-valid/1/" "paths-fan-in-single-routing-valid/exp-results/paths-[16-5]x5-fan-in-single-routing-valid/1/" "paths-fan-in-single-routing-valid/exp-results/paths-[16-5]x10-fan-in-single-routing-valid/1/" "paths-reconst-dropcircuit-fan-out-single-routing-valid/exp-results/paths-reconst-dropcircuit-fan-out-single-routing-valid-[16-5]x10/1/"
"paths-reconst-fan-out-single-routing-valid/exp-results/paths-reconst-fan-out-single-routing-valid-[16-5]x10/1/" "orig-fan-out-single-routing-valid/exp-results/no-reconst-capsnet-fan-out-single-routing-valid/1/" "orig-reconst-fan-out-single-routing-valid/exp-results/reconst-capsnet-fan-out-single-routing-valid/1/")

PATH_NUMS=(5 32 32 10 16 10 5 10 5 10 10 10 32 32)

EXPS_NAMES=("pathcapsnet-dropcircuit-fan-out-5-paths" "capsnet-reconst-fan-in" "capsnet-fan-in" "pathcapsnet-reconst-dropcircuit-fan-in-10-paths" "pathcapsnet-reconst-dropcircuit-fan-in-16-paths" "pathcapsnet-reconst-fan-in-10-paths" "pathcapsnet-dropcircuit-fan-in-5-paths" "pathcapsnet-dropcircuit-fan-in-10-paths" "pathcapsnet-fan-in-5-paths" "pathcapsnet-fan-in-10-paths" "pathcapsnet-reconst-dropcircuit-fan-out-10-paths" "pathcapsnet-reconst-fan-out-10-paths" "capsnet-fan-out"
"capsnet-reconst-fan-out")

CAPSNET_EXEC=$1
PATHCAPSNET_EXEC=$2
EXPS_PATH=$3
RESULTS_PATH=$4/subset_size_$5
SUBSET_SIZE=$5

capsnet(){

				EXP_PATH=$1
				EXP_ROOT=$2
				EXP_NAME=$3

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $CAPSNET_EXEC $EXP_ROOT $EXP_PATH/$t/model.pt $_RESULTS_PATH/corr.csv $SUBSET_SIZE
								fi

				done
}

pathcapsnet(){

				EXP_PATH=$1
				EXP_ROOT=$2
				EXP_NAME=$3
				IS_DROP=$4
				PATH_NUM=$5

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $PATHCAPSNET_EXEC $EXP_ROOT $EXP_PATH/config.json $EXP_PATH/$t/model.pt $_RESULTS_PATH/corr.csv $IS_DROP $PATH_NUM $SUBSET_SIZE

								fi
				done
}

for i in "${!EXPS[@]}";
do
				EXP_PATH="${EXPS[$i]}"
				EXP_ROOT=$(echo $EXP_PATH | awk -F/ '{print $1}')
				EXP_NAME="${EXPS_NAMES[$i]}"
				path_num="${PATH_NUMS[$i]}"

				if [[ $EXP_NAME == capsnet* ]];
				then

								capsnet $EXPS_PATH/$EXP_PATH $EXPS_PATH/$EXP_ROOT $EXP_NAME

				elif [[ $EXP_NAME == pathcapsnet* ]]
				then
								IS_DROP='False'

								if [[ $EXP_NAME == *dropcircuit* ]]
								then
												IS_DROP='True'
								fi

								pathcapsnet $EXPS_PATH/$EXP_PATH $EXPS_PATH/$EXP_ROOT $EXP_NAME $IS_DROP $path_num
				fi
done
