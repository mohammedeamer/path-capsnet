#!/bin/bash

set -e

CAPSNET_EXEC=$1
PATHCAPSNET_EXEC=$2
EXPS_PATH=$3
RESULTS_PATH=$4/subset_size_$5
SUBSET_SIZE=$5

EXPS_ROOTS=(v2-paths-dropcircuit-reconst-fan-in-mnist v2-paths-dropcircuit-reconst-fan-in-no-aug-mnist)

EXPS_NAMES=(v2-paths-dropcircuit-reconst-fan-in-mnist-16 v2-paths-dropcircuit-reconst-fan-in-no-aug-mnist-16)

capsnet(){

				EXP_PATH=$1
				EXP_NAME=$2

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $CAPSNET_EXEC $EXP_PATH $EXP_PATH/exp-results/$EXP_NAME/$t/model.pt $_RESULTS_PATH/corr.csv $SUBSET_SIZE
								fi

				done
}

pathcapsnet(){

				EXP_PATH=$1
				EXP_NAME=$2
				IS_DROP=$3

				for t in 1 2 3;
				do

								_RESULTS_PATH=$RESULTS_PATH/$EXP_NAME/$t

								if [[ ! -d $_RESULTS_PATH ]];
								then

									mkdir -p $_RESULTS_PATH

									python $PATHCAPSNET_EXEC $EXP_PATH $EXP_PATH/exp-results/$EXP_NAME/config.json  $EXP_PATH/exp-results/$EXP_NAME/$t/model.pt $_RESULTS_PATH/corr.csv $IS_DROP $SUBSET_SIZE

								fi
				done
}

for idx in ${!EXPS_ROOTS[@]};
do
				exp_root=$EXPS_PATH/${EXPS_ROOTS[$idx]}
				exp_name=${EXPS_NAMES[$idx]}

				if [[ -d $exp_root ]]
				then
								if [[ $exp_root == *orig* ]];
								then

												capsnet $exp_root $exp_name

								elif [[ $exp_root == *paths* ]]
								then
												IS_DROP='False'

												if [[ $exp_root == *dropcircuit* ]]
												then
																IS_DROP='True'
												fi

												pathcapsnet $exp_root $exp_name  $IS_DROP
								fi
				fi
done
