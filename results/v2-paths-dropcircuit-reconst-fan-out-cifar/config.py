
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3,
				'in_channels': 3,
				'paths_num': 16,
				'pathcaps_dim': 8,
				'digitcaps_dim': 16,
				'drop_prob': 0.5,
			}
}

