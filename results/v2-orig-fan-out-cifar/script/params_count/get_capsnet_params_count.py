import sys, os, argparse

import torch as tch

def run(exp_root):

	sys.path.insert(0, exp_root)

	from model import CapsNet

	model = CapsNet()

	params_count = tch.cat([p.view(-1) for p in model.parameters()], dim=0).size(0)

	exp_name = os.path.basename(exp_root)

	print('{} - params_count: {}'.format(exp_name, params_count))


if __name__ == '__main__':

	parser = argparse.ArgumentParser()

	parser.add_argument('exp_root', action='store')

	args = parser.parse_args()

	run(args.exp_root)

