import logging as log

from logger import MainLogger as disklog

import torch as tch
import utils

def infer_with_train_assist(model, perform_fn, train_ds, valid_ds):

	model.eval()

	loss_avg = utils.AverageMeter()

	batches_count = len(valid_ds)

	for b, (x, y) in enumerate(valid_ds):

		if tch.cuda.is_available():
			x = x.cuda()
			y = y.cuda()

		train_sample = list(next(iter(train_ds)))[0]

		train_x = train_sample[0].unsqueeze(0)
		if tch.cuda.is_available():
			train_x = train_x.cuda()

		aug_x = tch.cat([x, train_x], dim=0)

		o, _ = model(aug_x)

		loss = perform_fn(o[0], y)

		loss_avg.update(loss.item(), x.size(0))

		log.info("batch: {}/{} -- valid_loss: {}".format(b+1, batches_count, loss_avg.avg))

	disklog.log_test_perform(loss_avg.avg)

	return loss_avg.avg

