
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3,
				'in_channels': 1,
				'paths_num': 10,
				'pathcaps_dim': 8,
				'digitcaps_dim': 16,
			}
}

