
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3,
				'in_channels': 1,
				'paths_num': 16,
				'path_channels': [32,32,32,16],
				'pathcaps_dim': 16,
				'digitcaps_dim': 16,
				'drop_prob': 0.5,
			}
}

