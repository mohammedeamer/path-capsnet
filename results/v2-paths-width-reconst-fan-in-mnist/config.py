
CONFIG = {
	"exp_params": {
				"train_epochs": 300,
				'train_trials': 3,
				'in_channels': 1,
				'paths_num': 4,
				'path_channels': [64,64,64,32],
				'pathcaps_dim': 32,
				'digitcaps_dim': 16,
			}
}

