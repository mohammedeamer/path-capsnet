#!/bin/bash

set -e

# init

mkdir -p results/paths-reconst-dropcircuit-fan-in-single-routing-valid-dist/exp-results/pcn-reconst-[16-5]x5-dropcircuit-fan-in-single-routing-valid-dist

# run

sbatch --array=0-5 --export=EXP=paths-reconst-dropcircuit-fan-in-single-routing-valid-dist,SUBEXP=train,TAG=pcn-reconst-[16-5]x5-dropcircuit-fan-in-single-routing-valid-dist,ALL start_batch.sh
