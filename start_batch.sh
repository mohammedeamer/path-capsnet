#!/bin/bash
#SBATCH --time=100:00:00
#SBATCH --job-name=pcn
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --account=a14-0013
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hcxma1@nottingham.edu.my

module purge

srun --export=ALL start_job.sh
