import os, sys
import pandas as pd

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0, os.path.join(dir_path,'../../src/'))

from datautils import normalise

RAW_PATH = os.path.join(dir_path, '../../raw/mnist/')

DATA_PATH = os.path.join(dir_path, 'exp-data')

def build():

	if not os.path.exists(DATA_PATH):
		os.makedirs(DATA_PATH)

	names = ["class"]

	for i in range(784): names.append(str(i))

	train_df = pd.read_csv(os.path.join(RAW_PATH, "train.csv"), names=names)

	test_df = pd.read_csv(os.path.join(RAW_PATH, "test.csv"), names=names)

	names = names[1:] + names[0:1]

	train_df = normalise(train_df[names])

	test_df = normalise(test_df[names])

	train_df.to_csv(os.path.join(DATA_PATH, "train.csv"))
	test_df.to_csv(os.path.join(DATA_PATH, "test.csv"))

if __name__ == '__main__':
	build()