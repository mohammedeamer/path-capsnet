import os

import pandas as pd
from PIL import Image

CLS_SAMPLES_COUNT = 1000

def save_files(files, path):

	os.makedirs(path)

	size = (88, 64)

	for f in files:

		img = Image.open(f).convert('L')
		img.thumbnail(size, Image.ANTIALIAS)
		img.save(os.path.join(path, '{}.jpg'.format(os.path.basename(f).split('.')[0])), 'JPEG')

		print(f)

dir_path = os.path.dirname(os.path.realpath(__file__))
ds_path = os.path.join(dir_path, "../../raw/iwild")

data_path = os.path.join(dir_path, 'exp-data')

os.makedirs(data_path)

# ['category_id', 'date_captured', 'file_name', 'frame_num', 'id', 'location', 'rights_holder', 'seq_id', 'seq_num_frames', 'width', 'height']

meta_df = pd.read_csv(os.path.join(ds_path, 'meta.csv'))
meta_df['category_id'] = [int(categ) for categ in meta_df['category_id']]
meta_df = meta_df.drop_duplicates(subset=['file_name'])

classes = [1,3,4,10,11,13,14,17,18,19]

for cls in classes:

	cls_df = meta_df[meta_df['category_id'] == cls]

	train_count = int(CLS_SAMPLES_COUNT*0.7)
	valid_count = int(CLS_SAMPLES_COUNT*0.2)
	test_count = CLS_SAMPLES_COUNT - (train_count+valid_count)

	all_files = cls_df['file_name'].values
	all_files = [os.path.join(ds_path, f) for f in all_files]

	train_files = all_files[:train_count]
	valid_files = all_files[train_count:train_count+valid_count]
	test_files = all_files[train_count+valid_count:train_count+valid_count+test_count]

	save_files(train_files, os.path.join(data_path, 'train', str(cls)))
	save_files(valid_files, os.path.join(data_path, 'valid', str(cls)))
	save_files(test_files, os.path.join(data_path, 'test', str(cls)))
