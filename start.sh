#!/bin/bash

set -e

EXP=$2
DETACH=

if [ "$1" = 'run' ]; then
	DETACH=-d
fi

RUNTIME=

IMAGE_NAME='conv-caps'
DATA_PATH=$(pwd)/data/
RAW_PATH=$(pwd)/raw/
RESULTS_PATH=$(pwd)/results/

if [ $(command -v "nvidia-smi") ]; then
	RUNTIME='--runtime=nvidia'
	echo "Nvidia detected"
else
	echo "No Nvidia"
fi

docker build -t $IMAGE_NAME .

docker run -it $RUNTIME $DETACH -v $DATA_PATH:/usr/src/data/ -v $RAW_PATH:/usr/src/raw/ -v $RESULTS_PATH:/usr/src/results/ $IMAGE_NAME "${@:2}"
